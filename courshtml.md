# HTML

HTML est l'acronyme de *HyperText Mark-up Language* , inventé par Tim Berners-Lee en 1991.

Le langage HTML est interprété par les navigateurs internet. C'est un langage de description et non de programmation (pour le web php, scripts, cgi, javascript peuvent être utilisés pour faire des pages dynamiques, nous en reparlerons plus tard cette année).

Il repose sur la norme W3C. *(Le World Wide Web Consortium définit tous les standards du web, dans l'objectif d'harmoniser les usages, mais aussi d'améliorer l'accessibilité des sites web)*.

C'est un langage qui utilise des balises pour la mise en forme d'un texte.

On l'associe en général à des feuilles de styles CSS et à des scripts en javascript pour définir HTML5.

## Balises 

Le *balisage* d'une page HTML va délimiter et structurer les parties de cette page, en appliquant à chacune de ces parties diverses *propriétés* qui seront interprétées de manière différente par le navigateur selon le type de balises utilisées.

Les différentes contenus sont délimitées par une balise ouvrante et une balise fermante:\
` <mabalise> mon contenu </mabalise>`.

Seul le texte écrit entre les balises ouvrante et fermante s'affiche à l'écran. Une balise ouvrante et sa balise fermante correspondante forment donc pour ainsi dire un conteneur enserrant du contenu. Ces conteneurs s'imbriquent comme des poupées russes :\
`<exemple1>contenu<exemple2>contenu</exemple2></exemple1>`

Mais si les balises s'imbriquent, elles ne peuvent en revanche pas se croiser. Par exemple, une structure de ce type est interdite :\
`<exemple1>contenu<exemple2>contenu</exemple1></exemple2>`

Les marqueurs sont des balises auto-fermantes ou orphelines
`<monmarqueur/>`.

Les attributs (facultatifs) modifient une valeur de l'attribut:\
`<mabalise attribut=’valeur’>`

Par exemple, les attributs possibles de la balise `body` sont `text` (couleur du texte), `bgcolor` (couleur du fond), `background` (image du fond).

Pour l'instant nous utiliserons les mots anglais pour définir les couleurs mais nous verrons plus tard comment utiliser le code RVB.

## Validation

Une bonne imbrication des balises est une des règles à respecter afin d'avoir un code valide.

Le W3C propose un outil pour vérifier la validité de son code. Il listera vos erreurs jusqu'à temps que vous ayez une page valide :
<http://validator.w3.org/>

## Structure générale

Toute page commence par `<!DOCTYPE html >` et finit par `</html>`.

Le document est séparé en deux grandes parties:

-   le `head`: qui contient des informations utiles au navigateur et aux robots qui consultent votre page. Elle est délimitée par les balises `< head>` et `</head>`.
    -   le titre de la page, balise `< title>`,
    -   le nom de l'auteur, (facultatif)
    -   le mode d'encodage,
    -   le lien vers la feuille de style (nous en reparlerons très vite)

-   le `body`: qui contient les informations qui seront effectivement affichées à l'écran.


 <div style="page-break-after: always;"></div>

# Le CSS

Les principales balises du HTML permettent de créer une page web \"brute\" avec des liens mais dépouillée \...donc plutôt laide!

La mise en forme du texte se fait à laide des \"feuilles de style\" et du langage CSS (Cascade Style Sheets).

Il y a deux façons de procéder :
-   écrire les instructions de style dans la page html, avec la balise `< style>`
-   traiter les styles dans un fichier séparé, ce qui est de loin préférable, car avec un seul fichier on peut modifier la présentation de centaines de pages `html`.
	-   création un nouveau fichier `css` dans `Notepad++` (à enregistrer dans le même répertoire que le fichier `html`
    -   Ajout du lien vers la feuille de style entre les balises `< head>` et `</head>` de chaque page `html`
        `< link rel="stylesheet" href="monstyle.css" >`

Tester ce fichier `monstyle.css` sur votre page `html` précédente

```css
body{
	background-color: white;
	tex-align: center;
}

p{
	font-family: verdana;
	font-size: 20px;
}
```
![[AC9D.tmp.png]]

## Les identifiants

Parfois, on veut appliquer un style spécial à un élément ou à un groupe d'éléments particuliers. 

Il existe 2 types d'identifiants pour appliquer un style à une partie précise d'un document :
-   l'identifiant `id`, qui ne concerne qu'un élément unique,
-   l'identifiant `class`, qui peut s'appliquer à plusieurs éléments.

Exemple: On veut écrire en gras, plus gros et en bleu le paragraphe :
`< p> J’aime CSS</p>`

-   Dans le code `html`, dans la balise ouvrante de paragraphe, on ajoute
    `id="love"`: `< p id="love">J’aime CSS</p>`
-   Dans le fichier `css`, on ajoute:
    `#love{color:pink; font-weight:bold;}
	
Dans le fichier `css`:
-   Devant le nom de l'`id` on ajoute un dièse (#)
-   Devant le nom de la `class` on ajoute un point(.)