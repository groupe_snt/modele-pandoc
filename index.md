# Page d'accueil

Le titre de niveau 1 définit le titre de la page.

Voici un paragraphe de texte. On peut mettre du texte en **gras**, en *italique* ou en ***gras et italique***.

On peut aussi écrire des formules mathématiques $E=mc^2$

$$R_{\mu{}\nu{}} - \frac{1}{2}g_{\mu{}\nu{}} + \Lambda{}g_{\mu{}\nu{}}=\kappa{}T_{g_{\mu{}\nu{}}}$$

## Titre de niveau 2

On peut également créer des listes :
- Item 1
- Item 2
- Item 3

### Titre de niveau 3

Ou des listes numérotées :
1. Item 1
2. Item 2
3. Item 3

#### Titre de niveau 4

On peut insérer des liens : [Texte du lien](https://forge.apps.education.fr/)

Et des images :

![Légende de la photo](https://picsum.photos/id/17/2500/1667)

##### Titre de niveau 5

On peut également ajouter du code :

```python
print("Hello world!")
```

Ou du texte en ligne `comme ceci`.

###### Titre de niveau 6


> On peut aussi citer du texte.

Et enfin, on peut créer des tables :

| Colonne 1 | Colonne 2 |
|:-:|:-:|
| Ligne 1, Colonne 1 | Ligne 1, Colonne 2 |
| Ligne 2, Colonne 1 | Ligne 2, Colonne 2 |

---

## Syntaxe plus avancée

### Modifications du texte

On peut ++souligner++ du texte, ==surligner== du texte ou ~~barrer~~ du texte.

On peut aussi utiliser des exposants (19^th^) ou des indices (H~2~O)

### Ajouts dans le texte

On peut utiliser des codes pour insérer des emojis : :smile: :+1:

On peut écrire des définitions :

Terme à définir
: Définition du terme

On peut utiliser des notes[^1] de bas de page [^unenote]

[^1]: Ceci est la première note de bas de page.

[^unenote]: Voici la deuxième.

On peut utiliser des boîtes prédéfinies :

:::info
Boîte de contenu de type “Informations”
:::

:::danger
Boîte de contenu de type “Danger”
:::

:::success
Boîte de contenu de type “Succès”
:::


## Liste des pages de ce site

On peut enfin inclure la liste des pages du site (cette liste est générée automatiquement)

``` {.include}
links.html
```